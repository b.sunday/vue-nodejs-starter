import Vue from "vue";
import Vuex from "vuex";

// services

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    reports: [],
    roles: [],
    loader: false,
    isSearching: false,
    carModels: [],
    modalState: {
      value: false,
      message: "",
    },
    user: false,
  },
  mutations: {
    setUser(state, payload) {
      state.user = payload;
    },

    setReportsData(state, reports) {
      state.reports = reports;
    },

    setLoading(state, payload) {
      state.loader = payload;
    },

    toggleModal(state, payload) {
      state.modalState = {
        value: !state.modalState.value,
        message: payload,
      };
    },
    toggleSearch(state, payload) {
      state.isSearching = payload;
    },
  },
  actions: {
   
    isLoading({ commit }, payload) {
      commit("setLoading", payload);
    },

    toggleModal({ commit }, message) {
      commit("toggleModal", message);
    },

    toggleSearch({ commit }, payload) {
      commit("toggleSearch", payload);
    },
  },
  getters: {
    rolesPresent: (state) => state.roles.length > 0,
    isAdmin: (state) =>
      (state.user &&
        state.user.role["encodedKey"] == "8a858ee159de9b420159debe33340dda") ||
      false,
    getUser: (state) => state.user,
  },
  modules: {},
});
