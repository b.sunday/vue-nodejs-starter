const dev = {
    BACKEND_SERVICE: "http://localhost:3000/v1"
};

const prod = {
  // BACKEND_SERVICE: "https://lbfrefinance.platinumcredit.co.tz/v1",
  BACKEND_SERVICE: "https://dmv.platinumcredit.co.ug/lbf/v1",
};



const choose = {
    dev,
    prod
};


const config = process.env.VUE_APP_STAGE ? choose[process.env.VUE_APP_STAGE] : choose["prod"];

export default config;