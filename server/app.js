var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const apiRoutes = require("#routes/index");
const { session } = require("#utils/index");
const middlewares = require("#server/middleware");
const cors = require("cors");

// const validateRequestController = require("#controllers/validateRquest.controller");
const fronteend = __dirname + "/views/";

require("dotenv").config({ path: path.join(__dirname, "../../.env") });

var app = express();

app.use(express.static(fronteend));
app.use(logger("dev"));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "../public")));

// when using custom session
// app.use("/api/", validateRequestController.validateEntry);
// app.use("/api", validateRequestController.validateReqFiles);

app.post("/", (req, res, next) => {
  console.log(req.body);
  const _signed = req.body.signed_request;
  session(_signed);
  req.method = "GET";
  next();
});

app.get("/", function (req, res) {
  res.sendFile(fronteend + "index.html");
});

app.use("/v1", apiRoutes);

// when using custom session
// app.use("/validate", validateRequestController.validateReponse);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

module.exports = app;
