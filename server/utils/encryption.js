const { createHmac } = require("crypto");

const encryption = {};

encryption.createHash = function (string) {
  if (!process.env.app_key) {
    return false;
  }
  return createHmac("sha256", process.env.app_key).update(string).digest("hex");
};

module.exports = encryption;
