const fs = require("fs");
const path = require("path");

module.exports = (data) => {
  console.log("------------session saving------");
  const file = `
    function saveSession (val){
      localStorage.setItem('mambuUser', val);
      console.log(localStorage)
    }
    saveSession("${data}")`;

  try {
    fs.writeFileSync(path.join(__dirname, "../views/session.js"), file);
    ("------------session saved------");
  } catch (error) {
    console.log(error);
  }
};
