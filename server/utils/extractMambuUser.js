function extractMambuUser(
  signed_request
) {
  // split the base64 grab first item   
  let _signed = signed_request;
  _signed = signed_request.split(".")[1];

  // decode 
  const data = new Buffer.from(_signed, "base64").toString("utf8");

  // parse json
  /**
   * {DOMAIN: 'premierkenya.sandbox.mambu.com', ALGORITHM: 'hmacSHA256', TENANT_ID: 'premierkenya', USER_KEY: '8a9387a815017f02d7ba9e0026'}
   */
  const decode_obj = JSON.parse(data);

  const userId =  decode_obj["USER_KEY"];
  const base_url = "https://" + decode_obj['DOMAIN'] + '/api'

  return {userId, base_url};
}


module.exports = extractMambuUser;

