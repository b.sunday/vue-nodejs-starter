require("dotenv").config();
const sgMail = require("@sendgrid/mail");

const apikey = process.env.sendgrid_key;

sgMail.setApiKey(apikey);

module.exports = sendEmail;

async function sendEmail({
  to,
  subject,
  from = "b.sunday@platcorpgroup.com",
  ...rest
}) {
  try {
    const msg = {
      to,
      subject,
      from,
      ...rest,
    };

    await sgMail.send(msg);
  } catch (error) {
    throw error;
  }
}
