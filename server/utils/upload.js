const multer = require("multer");

const upload = multer({
  dest: "uploads/",
  limits: {
    fileSize: 1000000 * 100,
  },
  fileFilter(req, file, cb) {
    console.log(file.originalname);
    if (!file.originalname.match(/\.(xlsx)$/)) {
      return cb(new Error("Please upload an xlsx file"));
    }

    // else call cb with error ==undefined and file ==triue
    cb(undefined, true);
  },
});

module.exports = upload;
