const fs = require("fs");
const xlsx = require("xlsx");
const { promisify } = require("util");
const unlinkFile = promisify(fs.unlink);

// step 1
const loadXlsx = function ({ sheet_index = 0, filename }) {
   
  const pathToExcel = "uploads/" + filename ;
  try {
    let workbook = xlsx.readFile(pathToExcel);
    const sheet_list = workbook.SheetNames; // get sheet list names
    const sheet_to_json = xlsx.utils.sheet_to_json(
      workbook.Sheets[sheet_list[sheet_index]]
    );

    return { filename, data: sheet_to_json };
  } catch (error) {
    throw error;
  }
};

// step 2 write file to json
const saveFileToJson = function (filename, list) {
  const dataJson = JSON.stringify(list, null, 4);
  const _toSave = "uploads/" + filename + ".json";
  fs.writeFileSync(_toSave, dataJson, (err) => {
    if (err) throw new Error(err);
    console.log("File written succesfully");
  });
};

//step 3 load json data from file
const loadJson = function (filename) {
  /***
   * read already generated file and return the data
   * filename e.g "./xlsx.json"
   */
   const _toSave = "uploads/" + filename + ".json";
  try {
    const dataBuffer = fs.readFileSync(_toSave);
    const dataJson = dataBuffer.toString();
    return JSON.parse(dataJson);
  } catch (error) {
    // if no file
   throw error;
  }
};

//step 4 sanitizes the excel data
const sanitize = function (list) {
  // getting ["1", "Baringo County"]
  const controlObjKeys = Object.keys({ ...list[0] });
  const _sanitized = [];

  for (const item of list) {
    let center = {
      name: item[controlObjKeys[1]],
    };

    _sanitized.push(center);
  }

  return _sanitized;
};

// step 5 unlink file from system
const deleteFile = function (filename, ext = ".xlsx") {

  const toBeRemoved = ext == '.xlsx' ? "uploads/" + filename  : "uploads/" + filename + ext;
  return unlinkFile(toBeRemoved);
};

module.exports = {
  loadXlsx,
  saveFileToJson,
  loadJson,
  sanitize,
  deleteFile,
};
