const {
  ValidationError,
  BulkRecordError,
  ConnectionError,
  DatabaseError,
} = require("sequelize");

function notFound(req, res, next) {
  const error = new Error(`🔍 - Not Found - ${req.originalUrl}`);
  return res.status(404).json({
    message: error.message || err,
  });
}

/* eslint-disable no-unused-vars */
function errorHandler(err, req, res, next) {
  /* eslint-enable no-unused-vars */
  console.log(err);
  const statusCode = res.statusCode !== 200 ? res.statusCode : 500;
  res.status(statusCode);

  const seqErr = err.parent || err.original;

  if (err instanceof ValidationError) {
    switch (err.name) {
      case "SequelizeUniqueConstraintError":
        return res.status(400).send({
          message: seqErr,
          type: err.name,
          data: {},
        });
        break;
      default:
        res.status(400).send({
          message: seqErr,
          type: err.name,
        });
        break;
    }
  } else if (err instanceof BulkRecordError) {
    res.status(400).send({
      message: seqErr,
      type: err.name,
    });
  } else if (err instanceof ConnectionError) {
    switch (err.name) {
      case "SequelizeConnectionRefusedError":
        res.status(400).send({
          message: seqErr,
          type: err.name,
          data: {},
        });
        break;
      case "SequelizeConnectionTimedOutError":
        res.status(400).send({
          message: seqErr,
          type: err.name,
          data: {},
        });
        break;
      default:
        res.status(400).send({
          message: seqErr,
          type: err.name,
        });
        break;
    }
  } else if (err instanceof DatabaseError) {
    switch (err.name) {
      case "SequelizeForeignKeyConstraintError":
        res.status(400).send({
          message: seqErr,
          type: err.name,
          data: {},
        });
        break;
      default:
        res.status(400).send({
          message: seqErr,
          type: err.name,
        });
        break;
    }
  } else {
    switch (true) {
      case typeof err === "object":
        // custom application error
        const statusCode = 400;

        return res.status(statusCode).json({
          message: err.message || err,
        });

      default:
        return res.status(500).json({
          message: err.message || err,
        });
    }
  }
}

module.exports = {
  notFound,
  errorHandler,
};
