const extractMambuUser = require("#utils/extractMambuUser");

const auth = (req, res, next) => {
  let idToken = req.header("mambuUser");

  if (!idToken) {
    console.error("no token found");
    return res.status(403).json({ error: "No token found,Unauthorized" });
  }

  const { userId, base_url } = extractMambuUser(idToken);

  req.userId = userId;
  return next();
};

module.exports = auth;
