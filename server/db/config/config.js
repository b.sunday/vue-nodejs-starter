const dotenv = require("dotenv");
const path = require("path");
dotenv.config({ path: path.join(__dirname, "../../.env") });

module.exports = {
  development: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DBNAME,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT,
    // encrypt: process.env.DB_ENCRYPT,
    // pool: {
    //   max: parseInt(process.env.DB_POOL_MAX),
    //   min: parseInt(process.env.DB_POOL_MIN),
    //   acquire: parseInt(process.env.DB_POOL_ACQUIRE),
    //   idle: parseInt(process.env.DB_POOL_IDLE),
    // },
  },
  test: {
    use_env_variable: true,
    username: "sunday",
    password: "belter",
    database: "database_development",
    host: "127.0.0.1",
    dialect: "postgres",
  },
  production: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DBNAME,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT,
  },
};
