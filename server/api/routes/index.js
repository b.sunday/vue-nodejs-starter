var express = require('express');
var router = express.Router();
const userRoutes = require("#routes/users/users.routes");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.use("/users", userRoutes);

module.exports = router;
