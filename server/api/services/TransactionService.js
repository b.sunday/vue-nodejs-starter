const axios = require("axios").default;

class TransactionService {

    static async getDepositTransanction(id) {
        const deposit =await axios.get(`deposits/${id}/?detailsLevel=Full`);
        return deposit.data;

    }

    static async getWithdrawalTransaction(id) {
        const data = {
            "Transactions": [

                ['accountId', 'amount'],
                ['PK88434', '500'],
                ['PK51896', '200'],
                ['PK59687', '100']
            ]
        }
      

        const withdrawal = await axios.post(`withdrawal/${id}`, data);
        return withdrawal.data;
        
    }
};



