require("dotenv").config();
const createError = require("http-errors");

const saveSession = require("#utils/session");
const { createHash } = require("#utils/encryption");

const validateRequest = {};

validateRequest.validateEntry = function (req, res, next) {
  if (req.url === "/v1/approve-client") {
    next();
  } else {
    if (req.method === "POST") {
      console.log(req.body);
      const mambuSignedRequest = req.body.signed_request;
      if (!mambuSignedRequest) {
        next(createError(401));
      }
      const encodedString = mambuSignedRequest.split(".")[1];
      console.log("\n\n-----------------");
      console.log(createHash(encodedString));
      if (mambuSignedRequest.split(".")[0] === createHash(encodedString)) {
        console.log(createHash(encodedString));
        saveSession(mambuSignedRequest);
        req.requestIsValid = true;
        req.method = "GET";
        next();
      } else {
        next(createError(401));
      }
    } else {
      next();
    }
  }
};

validateRequest.validateReqFiles = function (req, res, next) {
  // use your use case req.url
  if (req.url === "/v1/approve-client") {
    next();
  } else {
    const ext = req.url.replace("/", ".").split(".").slice(-1)[0];
    const allowed = ["css", "js", "map"];
    const valid = req.requestIsValid;
    const mambuUser = req.header("mambuUser");
    console.log("extn", ext);
    console.log("extn", valid);
    console.log(mambuUser);
    if (allowed.includes(ext) || valid || mambuUser) {
      next();
    } else {
      console.log("here// file validation");
      next();
    }
  }
};

validateRequest.validateReponse = function (req, res, next) {
  const mambuUser = req.header("mambuUser");
  const encodedString = (mambuUser || "").split(".");
  if (encodedString[0] !== createHash(encodedString[1])) {
    next(createError(404));
  }
  next();
};

module.exports = validateRequest;
