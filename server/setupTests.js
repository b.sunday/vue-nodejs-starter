const db = require("#models/index");
const { Umzug, SequelizeStorage } = require("umzug");
const path = require("path");
const Sequelize = require("sequelize");
const { sequelize } = db;

const umzug = new Umzug({
  migrations: {
    glob: path.join(__dirname, "/db/migrations/*.js"),
    resolve: ({ name, path, context }) => {
      const migration = require(path);
      return {
        // adjust the parameters Umzug will
        // pass to migration methods when called
        name,
        up: async () => migration.up(context, Sequelize),
        down: async () => migration.down(context, Sequelize),
      };
    },
  },
  context: sequelize.getQueryInterface(),
  storage: new SequelizeStorage({ sequelize }),
  logger: console,
});

const seeders = new Umzug({
  migrations: {
    glob: path.join(__dirname, "/db/seeders/*.js"),
    resolve: ({ name, path, context }) => {
      const migration = require(path);
      return {
        // adjust the parameters Umzug will
        // pass to migration methods when called
        name,
        up: async () => migration.up(context, Sequelize),
        down: async () => migration.down(context, Sequelize),
      };
    },
  },
  context: sequelize.getQueryInterface(),
  storage: new SequelizeStorage({ sequelize }),
  logger: console,
});

async function seed() {
  return seeders.up();
}

async function migrate() {
  return umzug.up();
}

async function revert() {
  return umzug.down({ to: 0 });
}

module.exports = async () => {
  // await db.sequelize.sync({ force: true });
  await revert();
  await migrate();
  await seed();
};
