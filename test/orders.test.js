const request = require("supertest");
const app = require("../server/app");

let order;
let customer = {
  id: 1,
  uuid: "e44852ab-6f15-4227-93b7-118de6cc8de4",
  receiver_phone: "07123456789",
  receiver_lat: 31,
  receiver_long: -1,
  receiver_name: "sunday omwami",
  uuid: "72c7054e-776b-4b56-8879-46eca3684665",
};
const merchant = {
  sender_lat: -1.2628519,
  sender_long: 36.8063698,
  sender_phone: "0791688152",
  sender_id: "38bf48f0-a699-11ec-a4ab-713c9a501d3a",
  sender_name: "Confini",
  delivery_settings: "not_set",
  payment_instructions: null,
  uuid: "38bf48f0-a699-11ec-a4ab-713c9a501d3a",
  id: 1,
};
const product = {
  id: 1,
  uuid: "a173992c-76f0-4d85-aa15-a0c90487e542",
  product_name: "shoe plug",
  product_img: "string",
  product_quantity: 3,
  product_price: 89.45,
  merchantId: 1,
  categoryId: 1,
};

describe("POST /v1/orders/placeOrder/:merchantUuid", () => {
  it("should create an order for customer", async () => {
    const res = await request(app)
      .post(`/v1/orders/placeOrder/${merchant.uuid}`)
      .send({
        receiver_lat: customer.receiver_lat,
        receiver_long: customer.receiver_long,
        receiver_phone: customer.receiver_phone,
        receiver_name: customer.receiver_name,
        cart: [
          {
            productUuid: product.uuid,
            quantity: 10,
            price: 89.45,
          },
        ],
      })
      .expect("Content-Type", /json/)
      .expect(200);
  });
});

//"merchantUuid

describe("GET /v1/orders/fetchOrdersMerchant/?merchantUuid", () => {
  it("should fetch orders for a merchant", async () => {
    const res = await request(app)
      .get(
        `/v1/orders/fetchOrdersMerchant?merchantUuid=${merchant.uuid}&status=all`
      )
      .expect("Content-Type", /json/)
      .expect(200);

    expect(res.body.rows.length).toBeGreaterThan(0);
  });
});

describe("GET /v1/orders/fetchOrdersCustomer/?phone", () => {
  it("should fetch orders for a merchant", async () => {
    const res = await request(app)
      .get(
        `/v1/orders/fetchOrdersCustomer?phone=${customer.receiver_phone}&status=all`
      )
      .expect("Content-Type", /json/)
      .expect(200);
    expect(res.body.rows.length).toBeGreaterThan(0);
  });
});

//4d174360-781c-4235-9150-41e6c08b29a7
describe("GET /v1/orders/uuid", () => {
  it("should fetch orders for a merchant", async () => {
    const res = await request(app)
      .get(`/v1/orders/4d174360-781c-4235-9150-41e6c08b29a7`)
      .expect("Content-Type", /json/)
      .expect(200);

    // expect(res.body.rows.length).toBeGreaterThan(0);
  });
});
