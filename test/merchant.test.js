const request = require("supertest");
const app = require("../server/app");

const merchant = {
  sender_lat: -1.2628519,
  sender_long: 36.8063698,
  sender_phone: "0891688152",
  sender_id: "68bf48f0-a699-11ec-a4ab-713c9a501d3a",
  sender_name: "Confini 22",
};

describe("POST /v1/merchants", () => {
  it("should create a merchant", async () => {
    const res = await request(app)
      .post(`/v1/merchants`)
      .send({
        ...merchant,
      })
      .expect("Content-Type", /json/)
      .expect(200);

    expect(res.body.sender_phone).toBe("0891688152");
  });
});
