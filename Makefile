dev:
	yarn run dev
amka:
	sudo pg_ctlcluster 10 main start
restart:
	sudo service postgresql restart	
migrate:
	npx sequelize-cli db:migrate
pm2:
	pm2 start ecosystem.config.js	
kill:
	sudo kill -9 `sudo lsof -t -i:3001`
serve:
	yarn run server	
redis:
	sudo service redis-server start	
push:
	git push origin master
build:
	cd frontend && yarn run build
historia:
	git pull origin main --allow-unrelated-histories
undo:
	npx sequelize-cli db:migrate:undo
undo_all:
	npx sequelize-cli db:migrate:undo:all
_test:
	dropdb insertdbName && createdb insertdbName && yarn run test 	

	