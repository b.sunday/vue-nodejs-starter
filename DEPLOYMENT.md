# Deploy Nodejs App

## Building Static assest

- run ` make build` to build frontend assests
- access the views dir, then navigate access index.html
- format the document , and ensure all files have relative imports. eg
  instead of /css/about.css change to ./css/about.css

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <link rel="icon" href="/favicon.ico" />
    <title>lbfdashboard</title>
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900"
    />
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/@mdi/font@latest/css/materialdesignicons.min.css"
    />
    <link href="./css/about.css" rel="prefetch" />
    <link href="./js/about.js" rel="prefetch" />
    <link href="./css/app.css" rel="preload" as="style" />
    <link href="./css/chunk-vendors.css" rel="preload" as="style" />
    <link href="./js/app.js" rel="preload" as="script" />
    <link href="./js/chunk-vendors.js" rel="preload" as="script" />
    <link href="./css/chunk-vendors.css" rel="stylesheet" />
    <link href="./css/app.css" rel="stylesheet" />
  </head>
  <body>
    <noscript
      ><strong
        >We're sorry but lbfdashboard doesn't work properly without JavaScript
        enabled. Please enable it to continue.</strong
      ></noscript
    >
    <div id="app"></div>
    <script src="./session.js"></script>
    <script src="./js/chunk-vendors.js"></script>
    <script src="./js/app.js"></script>
  </body>
</html>
```

- ensure you append `./` to your scripts and css

## Assumptions

- Your familiar with the basics of nodejs
- You have nodejs application that you want to deploy to linux server
- You are familiar with package.json, npm or yarn - not a must but the knowledge would be a plus
- Your deployment db is postgres or mysql or whatever rocks your boat

## windows and wsl user

You need to move the pem key from whatever DIR to you ubuntu `.ssh` folder. Assumptions here is the Downloads Folder, Please note your credentials will be different.

- Open CMD and type ``wsl`
- `sudo ls /mnt` to check windows dir
- `cp /mnt/c/Users/ENVY/Downloads/dev_server_pclk.pem /home/sundaypriest/.ssh` copy the pem file from path the exact location to your ubuntu .ssh dir
- ` chmod 400 /home/sundaypriest/.ssh/dev_server_pclk.pem` set permission on key
- `ssh -i /home/sundaypriest/.ssh/dev_server_pclk.pem ubuntu@ec2-1-117-12-112 ` login to your instance server

# Preparing The Instance

Run the following steps in order to install the neccessary packages

1.  run `sudo apt-get update -y`
2.  run `sudo apt-get install curl apt-transport-https gnupg2 wget build-essential unzip nano -y`
3.  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
4.  close terminal and exit the instance by typing `exit`
5.  `ssh -i /home/sundaypriest/.ssh/dev_server_pclk.pem ubuntu@ec2-1-117-12-112 ` login to your instance again
6.  nvm install --lts
7.  `node -v` check node version
8.  `npm i -g yarn` to install yarn
9.  `npm install pm2 -g` to install pm2
10. sudo apt install python3-pip python3-dev libpq-dev postgresql postgresql-contrib nginx curl
11. sudo -u postgres psql
12. create database dbname
13. CREATE USER yourUserName WITH PASSWORD 'securepassword';
14. ALTER ROLE yourUserName SET client_encoding TO 'utf8';
15. ALTER ROLE yourUserName SET default_transaction_isolation TO 'read committed';
16. `ALTER ROLE yourUserName SET timezone TO 'Africa/Kampala'` set timezone according to your application location;
17. GRANT ALL PRIVILEGES ON DATABASE dbname TO yourUserName;
18. run `\q` to exit the psql shell
19. npm install pm2@latest -g
20. sudo ufw allow ssh
21. sudo ufw allow 'Nginx HTTP'
22. sudo systemctl status nginx
23. sudo service nginx restart

## Application Directories

18. sudo mkdir -p /var/www/yourAppName/html
19. sudo chmod -R 755 /var/www/yourAppName
20. sudo chown -R $USER:$USER /var/www/yourAppName/html
21. `sudo nano /etc/nginx/sites-available/yourAppName` add the nginx config

```
  server {
    client_max_body_size 10M;
    server_name my.subdomain.com;

    location /xml/ {
        root /var/www/yourAppName/html;
        index dev.xml;
    }

    location / {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_pass http://127.0.0.1:3000/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }
}
```

21. `CTRL + x` THEN `Y` to save the config
22. sudo ln -s /etc/nginx/sites-available/yourAppName /etc/nginx/sites-enabled/
23. `sudo nginx -t` check nginx block for errors
24. sudo service nginx restart
25. If you plan to upload files larger than 10mb via a ui or app, do change `client_max_body_size 10M;` to what you deem appropriate

# Setup Buddy CI

For more information read on pm2 [Buddy](https://buddy.works/).

- Authenticate Buddy using Gitlab Option
- Select Create a new project
- Click add new pipeline
- Give your pipeline a name
- select actions
- Next select sftp option under suggestions
- under source select gitlab repository
- Next scroll down to authentication mode choose `Private SSH key`
- Enter your instance ip in the hostname & port input
- The port set to 22
- The Login use your instance username e.g `ubuntu`
- For the ssh contents, navigate to your .ssh dir in your local machine
- `cd ~/.ssh `
- run `cat your_pem.key` to show contents of your pemkey
- copy the contents and paste in buddy under ssh key contents
- for the remote path, click browse and navigate to /var/www/yourAppName/html
- Select `use the above opened path`
- Click add this action

Now, everytime buddy your push or merge to the main branch, buddy will also send the changes to your instance

## Ecosystem File

An ecosystem file is already provided and setup, nothing is required on your part. You may optionally choose to change a few variables like the name attribute to match that of your application. This file is used by pm2 to run your app as a daemon

```
  module.exports = {
  apps: [
    {
      name: "yourAppName",
      script: "yarn start",
      watch: true,
      // Delay between restart
      watch_delay: 1000,
      ignore_watch: [
        "node_modules",
        "public",
        "error-logs",
        "pm2-logs",
        "src/views/js/session.js",
      ],
      watch_options: {
        followSymlinks: false,
      },
      env: {
        NODE_ENV: "development",
        TZ: "Africa/Kampala",
      },
      env_production: {
        NODE_ENV: "production",
        TZ: "Africa/Kampala",
      },
      error_file: "pm2-logs/err.log",
      out_file: "pm2-logs/out.log",
      log_file: "pm2-logs/combined.log",
      log_date_format: "YYYY-MM-DD HH:mm:ss Z",
    },
  ],
};

```

For more information read on pm2 [pm2 documentation](https://pm2.keymetrics.io/). 23. ssh into your instance 23. nano .env to add your environment variables 24. javigate to ` $ cd /var/www/yourAppName/html`` and run  `nano .env `and add your env variables for your application. Save file 24.`make migrate`To run migrations on your db 24. run`pm2 start ecosystem.config.js `to launch your application. Note the pm2 will assign the node instance the name defined in your ecosystem.config.js file 25. run`pm2 logs nameInEcosystemFile`to see your app logs 26. if you need to restart your app due to some issue run`pm2 restart nameInEcosystemFile```

# Adding https

- sudo snap install core; sudo snap refresh core
- sudo snap install --classic certbot
- sudo ln -s /snap/bin/certbot /usr/bin/certbot
- sudo certbot --nginx
  - follow the prompts to conclusion

# Other Installation

This process is for scenarios you neeed to use redis or chromium, if not, this part can be skipped.

### install chromium

For more information read on pm2 [Chromium](https://geekflare.com/install-chromium-ubuntu-centos/).

1.sudo apt-get update
2.sudo apt-get install -y libappindicator1 fonts-liberation 3. sudo apt-get install -f 4. wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb 5. sudo dpkg -i google-chrome\*.deb 6. google-chrome-stable -version  
7. if any error arises, just follow the ubuntu error message to sort it out

### install redis

1. sudo apt install redis-server
2. sudo systemctl restart redis.service
3. sudo systemctl status redis  


This process was a lot and you can easily mess up as I did, I suggest analyzing the part where you went wrong and going back to read on what you are supposed to do. I also highly recommend going through official documentations about deploying nodejs projects to linux instances as you will get a lot information that can help you debug effectively.

# Resources

## very helpfull articles

- https://medium.com/swlh/deploy-https-node-postgres-redis-react-to-aws-ef252567200d

## Future Plans

### Frontend

- Add more reusable components, currently we have
  - modal
  - loader
  - drawer
- Add Typescript
- Add StoryBook

### Backend

- Add Typescript
- Use gitlab runners for ci/cd
- Use docker containers
- Migrate from 3 tier folder structure to Clean Architecture
